package com.almerin.metadata.tool.entityproviders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sakaiproject.entitybroker.EntityReference;
import org.sakaiproject.entitybroker.entityprovider.capabilities.AutoRegisterEntityProvider;
import org.sakaiproject.entitybroker.entityprovider.capabilities.RESTful;
import org.sakaiproject.entitybroker.entityprovider.capabilities.RequestAware;
import org.sakaiproject.entitybroker.entityprovider.extension.Formats;
import org.sakaiproject.entitybroker.entityprovider.extension.RequestGetter;
import org.sakaiproject.entitybroker.entityprovider.search.Restriction;
import org.sakaiproject.entitybroker.entityprovider.search.Search;
import org.sakaiproject.entitybroker.util.AbstractEntityProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.almerin.metadata.logic.MetaDataService;
import com.almerin.metadata.logic.SakaiProxy;
import com.almerin.metadata.model.MetaData;

import lombok.Setter;

public class MetaDataEntityProvider extends AbstractEntityProvider
		implements RESTful, AutoRegisterEntityProvider, RequestAware {

	private RequestGetter requestGetter = null;

	public final static String METADATA_PREFIX = "almerin-metadata";

	public static final String REFERENCE_ROOT = "/metadata";

	private static Logger log = LoggerFactory.getLogger(MetaDataEntityProvider.class);

	@Setter
	private MetaDataService metadataService;

	@Setter
	private SakaiProxy sakaiProxy;

	@Override
	public String getEntityPrefix() {
		return METADATA_PREFIX;
	}

	@Override
	public Object getSampleEntity() {
		return new MetaData();
	}

	@Override
	public String createEntity(EntityReference ref, Object entity, Map<String, Object> params) {
		if (log.isDebugEnabled()) {
			log.debug("Add MetaData: " + ref);
			log.debug("Object: " + entity);
			log.debug("params: " + params);
		}

		if (MetaData.class.isAssignableFrom(entity.getClass())) {
			MetaData entityData = (MetaData) entity;
			if (isAllowedToAdd()) {
				if (metadataService.add(entityData)) {
					return entityData.getId().toString();
				} else {
					return "fail";
				}
			} else
				throw new IllegalArgumentException("Not Allowed To ADD MetaData");
		} else {
			throw new IllegalArgumentException("Invalid entity for CREATE, must be valid MetaData object");
		}
	}

	@Override
	public void updateEntity(EntityReference ref, Object entity, Map<String, Object> params) {
		if (log.isDebugEnabled()) {
			log.debug("Update MetaData: " + ref);
			log.debug("Object: " + entity);
			log.debug("params: " + params);
		}
		if (MetaData.class.isAssignableFrom(entity.getClass())) {
			MetaData newData = (MetaData) entity;
			String dataId = ref.getId();
			if (isAlloweToUpdate()) {
				MetaData realData = metadataService.getById(Long.valueOf(dataId));
				if (realData != null) {
					setFields(newData, realData);
					metadataService.update(realData);
				}
			} else {
				throw new IllegalArgumentException("Not Allowed To UPDATE MetaData");
			}
		} else {
			throw new IllegalArgumentException("Invalid entity for UPDATE, must be valid MetaData object");
		}
	}

	private void setFields(MetaData newData, MetaData realData) {
		realData.setObjectId(newData.getObjectId());
		realData.setAuthor(newData.getAuthor());
		realData.setDescription(newData.getDescription());
		realData.setKeywords(newData.getKeywords());
		realData.setLanguage(newData.getLanguage());
		realData.setMetameta(newData.getMetameta());
		realData.setEducational(newData.getEducational());
		realData.setTechnical(newData.getTechnical());
	}

	private boolean isAlloweToUpdate() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public MetaData getEntity(EntityReference ref) {
		if (log.isDebugEnabled()) {
			log.debug("Get MetaData: " + ref);
		}
		String dataId = ref.getId();
		if (isAllowedRead()) {
			MetaData data = metadataService.getById(Long.valueOf(dataId));
			return data;
		} else
			throw new IllegalArgumentException("Not Allowed To READ MetaData");
	}

	@Override
	public List<MetaData> getEntities(EntityReference ref, Search search) {
		if (log.isDebugEnabled()) {
			log.debug("EntityReference: " + ref);
			log.debug("Search: " + search);
		}

		String objectId = null;
		if (!search.isEmpty()) {
			Restriction objectIdRest = search.getRestrictionByProperty("objectId");
			if (objectIdRest != null) {
				objectId = objectIdRest.getStringValue();
			}

		}
		List<MetaData> list = new ArrayList<>();
		if (objectId == null || "".equals(objectId)) {
			return null;
		} else {
			list.add(metadataService.getByObjId(objectId));
		}
		return list;
	}

	@Override
	public void deleteEntity(EntityReference ref, Map<String, Object> params) {
		if (log.isDebugEnabled()) {
			log.debug("Delete MetaData: " + ref);
			log.debug("params: " + params);
		}
		String dataId = ref.getId();
		if (isAllowedToDelete()) {
			metadataService.delete(Long.valueOf(dataId));
		} else
			throw new IllegalArgumentException("Not Allowed To DELETE MetaData");
	}

	private boolean isAllowedToAdd() {
		// TODO Auto-generated method stub
		return true;
	}

	private boolean isAllowedRead() {
		// TODO Auto-generated method stub
		return true;
	}

	private boolean isAllowedToDelete() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String[] getHandledOutputFormats() {
		return new String[] { Formats.XML, Formats.JSON, Formats.HTML };
	}

	@Override
	public String[] getHandledInputFormats() {
		return new String[] { Formats.XML, Formats.JSON, Formats.HTML };
	}

	@Override
	public void setRequestGetter(RequestGetter requestGetter) {
		this.requestGetter = requestGetter;
	}

}
