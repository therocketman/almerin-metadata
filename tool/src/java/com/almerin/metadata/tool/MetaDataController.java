package com.almerin.metadata.tool;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.almerin.metadata.logic.SakaiProxy;

import lombok.Getter;
import lombok.Setter;

public class MetaDataController implements Controller {

	/**
	 * MetadataController
	 */
	@Setter
	@Getter
	private SakaiProxy sakaiProxy = null;

	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		return new ModelAndView("index");
	}

}
