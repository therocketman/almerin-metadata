<!DOCTYPE html>
<html ng-app="almerinApp">
<head>
<title>Almerin MetaData tool</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div ng-controller="AlmerinController as root">
		<metadata allowed="root.allowed"></metadata>
	</div>
</body>
</html>
