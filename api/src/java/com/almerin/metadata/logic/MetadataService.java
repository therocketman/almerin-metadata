package com.almerin.metadata.logic;

import com.almerin.metadata.model.MetaData;


public interface MetaDataService {

	public boolean add(MetaData data);

	public boolean delete(long dataId);

	public boolean update(MetaData data);

	public MetaData getById(long dataId);

	public MetaData getByObjId(String objectId);

}
