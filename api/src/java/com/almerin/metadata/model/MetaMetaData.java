package com.almerin.metadata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//@Entity
//@Table(name = "almerin_meta_metadata_t")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetaMetaData implements Serializable {
	
	private static final long serialVersionUID = 786975677009936653L;

	//@Id
	//@Column(name = "METADATA_ID")
	private Long id;

	//@Column(name = "METADATA_SCHEME")
	private String metadatascheme;
	
	//@MapsId
	//@OneToOne
	//@JoinColumn(name = "METADATA_ID")
	//private MetaData metadata;

}
