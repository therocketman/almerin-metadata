package com.almerin.metadata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//@Entity
//@Table(name = "almerin_metadata_t")

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetaData implements Serializable {

	private static final long serialVersionUID = 1367133628464512977L;
	//@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@Column(name = "METADATA_ID")
	private Long id;
	//@Column(name = "OBJECT_ID", unique = true, nullable = false, length = 255)
	private String objectId;
	//@Column(name = "TITLE")
	private String title;
	//@Column(name = "LANGUAGE")
	private String language;
	//@Column(name = "DESCRIPTION")
	private String description;
	//@Column(name = "AUTHOR")
	private String author;
	//@Column(name = "KEYWORDS")
	private String keywords;

	//@OneToOne(cascade = CascadeType.ALL)
	//@PrimaryKeyJoinColumn
	private TechnicalMetaData technical;

	//@OneToOne(cascade = CascadeType.ALL)
	//@PrimaryKeyJoinColumn
	private EducationalMetaData educational;

	//@OneToOne(cascade = CascadeType.ALL)
	//@PrimaryKeyJoinColumn
	private MetaMetaData metameta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public TechnicalMetaData getTechnical() {
		return technical;
	}

	public void setTechnical(TechnicalMetaData technical) {
		this.technical = technical;
	}

	public EducationalMetaData getEducational() {
		return educational;
	}

	public void setEducational(EducationalMetaData educational) {
		this.educational = educational;
	}

	public MetaMetaData getMetameta() {
		return metameta;
	}

	public void setMetameta(MetaMetaData metameta) {
		this.metameta = metameta;
	}
	
	@Override
	public String toString(){
		return "METADATA";
	}

}
