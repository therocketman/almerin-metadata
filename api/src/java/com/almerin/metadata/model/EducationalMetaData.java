package com.almerin.metadata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//@Entity
//@Table(name = "almerin_educational_metadata_t")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EducationalMetaData implements Serializable {

	private static final long serialVersionUID = 3639907213219394033L;
	// @Id
	// @Column(name = "METADATA_ID")
	private Long id;
	// @Column(name = "LEARNING_RES_TYPE")
	private String learningResourceType;
	// @Column(name = "CONTEXT")
	private String context; // subject
	// @Column(name = "TYPICAL_PAGE_RANGE")
	private String typicalPageRange;
	// @Column(name = "DIFFICULTY")
	private String difficulty;
	// @Column(name = "DESCRIPTION")
	private String description;
	// @Column(name = "LANGUAGE")
	private String language;
	// @Column(name = "INTERACTING_TYPE")
	private String interactingType;
	// @Column(name = "LEARNING_STYLE")
	private String learningStyle;

	// @MapsId
	// @OneToOne
	// @JoinColumn(name = "METADATA_ID")
	//private MetaData metadata;

}
