package com.almerin.metadata.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// @Entity
// @Table(name = "almerin_technical_metadata_t")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TechnicalMetaData implements Serializable {

	private static final long serialVersionUID = 4663190611259312852L;
	// @Id
	// @Column(name = "METADATA_ID")
	private Long id;
	// @Column(name = "FORMAT")
	private String format;
	// @Column(name = "SIZE")
	private String size;
	// @Column(name = "Type")
	private String type;
	// @MapsId
	// @OneToOne
	// @JoinColumn(name = "METADATA_ID")
	//private MetaData metadata;
	
	
}
