package com.almerin.metadata.logic.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.almerin.metadata.dao.MetaDataDao;
import com.almerin.metadata.logic.MetaDataService;
import com.almerin.metadata.logic.SakaiProxy;
import com.almerin.metadata.model.MetaData;

import lombok.Setter;

public class MetaDataServiceImpl implements MetaDataService {

	private static final Logger log = LoggerFactory.getLogger(MetaDataServiceImpl.class);

	@Setter
	private SakaiProxy sakaiProxy;

	@Setter
	private MetaDataDao dao;

	public void init() {
		log.debug("init");
	}

	@Override
	public boolean add(MetaData data) {
		return dao.addMetaData(data);
	}

	@Override
	public boolean delete(long dataId) {
		return dao.deleteMetaData(dataId);
	}

	@Override
	public boolean update(MetaData data) {
		return dao.updateMetaData(data);
	}

	@Override
	public MetaData getById(long dataId) {
		return dao.getMetaDataById(dataId);
	}

	@Override
	public MetaData getByObjId(String objectId) {
		return dao.getMetaDataForObj(objectId);
	}

}
