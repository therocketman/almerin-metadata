package com.almerin.metadata.dao;

import org.sakaiproject.genericdao.api.GeneralGenericDao;

import com.almerin.metadata.model.MetaData;

public interface MetaDataDao extends GeneralGenericDao {

	public boolean addMetaData(MetaData data);

	public boolean deleteMetaData(long dataId);

	public boolean updateMetaData(MetaData data);

	public MetaData getMetaDataById(long dataId);

	public MetaData getMetaDataForObj(String objectId);

}
