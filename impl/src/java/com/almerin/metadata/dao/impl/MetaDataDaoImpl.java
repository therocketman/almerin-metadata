package com.almerin.metadata.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.sakaiproject.genericdao.hibernate.HibernateGeneralGenericDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import com.almerin.metadata.dao.MetaDataDao;
import com.almerin.metadata.model.MetaData;

public class MetaDataDaoImpl extends HibernateGeneralGenericDao implements MetaDataDao {

	private static final Logger log = LoggerFactory.getLogger(MetaDataDaoImpl.class);

	/**
	 * init - perform any actions required here for when this bean starts up
	 */
	public void init() {
		log.info("init");
	}

	@Override
	public boolean addMetaData(MetaData data) {
		if (log.isDebugEnabled()) {
			log.debug("addMetaData: " + data);
		}
		try {
			getHibernateTemplate().save(data);
			return true;
		} catch (DataAccessException e) {
			log.error("addMetaData failed", e);
			return false;
		}
	}

	@Override
	public boolean deleteMetaData(long dataId) {
		if (log.isDebugEnabled()) {
			log.debug("deleteMetaData: " + dataId);
		}
		try {
			DetachedCriteria d = DetachedCriteria.forClass(MetaData.class).add(Restrictions.eq("id", dataId));
			List l = getHibernateTemplate().findByCriteria(d);
			if (l != null && l.size() > 0) {
				MetaData data = (MetaData) l.get(0);
				getHibernateTemplate().delete(data);
				return true;
			} else {
				log.error("deleteMetaData failed because none found wiht this id");
				return false;
			}
		} catch (DataAccessException e) {
			log.error("deleteMetaData failed", e);
			return false;
		}
	}

	@Override
	public boolean updateMetaData(MetaData data) {
		if (log.isDebugEnabled()) {
			log.debug("updateMetaData:  " + data);
		}
		try {
			getHibernateTemplate().merge(data);
			return true;
		} catch (DataAccessException e) {
			log.error("updateMetaData failed", e);
			return false;
		}
	}

	@Override
	public MetaData getMetaDataById(long dataId) {
		if (log.isDebugEnabled()) {
			log.debug("getMetaDataById(DataId: " + dataId + ")");
		}
		try {
			DetachedCriteria d = DetachedCriteria.forClass(MetaData.class).add(Restrictions.eq("id", dataId));
			List l = getHibernateTemplate().findByCriteria(d);
			if (l != null && l.size() > 0) {
				return (MetaData) l.get(0);
			} else {
				log.error("getMetaDataById() no MetaData for ID: " + dataId + ")");
				return null;
			}
		} catch (DataAccessException e) {
			log.error("getMetaDataById failed", e);
			return null;
		}
	}

	@Override
	public MetaData getMetaDataForObj(String objectId) {
		if (log.isDebugEnabled()) {
			log.debug("getMetaDataForObj(DataId: " + objectId + ")");
		}
		try {
			DetachedCriteria d = DetachedCriteria.forClass(MetaData.class).add(Restrictions.eq("objectId", objectId));
			List l = getHibernateTemplate().findByCriteria(d);
			if (l != null && l.size() > 0) {
				return (MetaData) l.get(0);
			} else {
				log.error("getMetaDataForObj() no MetaData for Object: " + objectId + ")");
				return null;
			}
		} catch (DataAccessException e) {
			log.error("getMetaDataForObj failed", e);
			return null;
		}
	}

}
